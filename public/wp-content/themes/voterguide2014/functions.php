<?php

if(!function_exists("voterguide_get_page_title"))
{
    function voterguide_get_page_title()
    {
        if(function_exists("is_tag") && is_tag()) {
            $pg_title = single_tag_title("Tag Archive for \"",false)."\" - ";
        } elseif(is_archive()) {
           $pg_title =  wp_title("",false)." Archive - ";
        } elseif(is_search()) {
            $pg_title = "Search for \"".$s."\" - ";
        } elseif(is_404()) {
            $pg_title = "Not Found - ";
        } elseif(is_single()||is_page()) {
            $pg_title = wp_title("",false)." - ";
        } else {
            $pg_title = "";
        }
        $pg_title .= bloginfo("name").((is_home())?" - ".bloginfo("description"):"").(($paged>1)?" - Page ".$paged:"");
        return htmlentities($page_title);
    }
}

if(!function_exists("voterguide_get_breadcrumbs"))
{
    function voterguide_get_breadcrumbs()
    {
        $breadcrumbs = [];

        if(function_exists('yoast_breadcrumb')) { // Uses WordPress SEO by Yoast Plugin

            $breadcrumbs['html'] = str_replace("\t","",str_replace("\n","",str_replace("\r\n","\n",
                trim(yoast_breadcrumb("","",false))
            )));

            $breadcrumbs['trail'] = array_map(function($e) {
                return trim($e);
            },explode(utf8_encode("�"),strip_tags($breadcrumbs['html'])));

            return $breadcrumbs;

        }

        return $breadcrumbs;
    }
}

if(!function_exists("array_keys_exist"))
{
    function array_keys_exist(array $keys,array $arr)
    {
        foreach($keys as $k) {
            if(!isset($arr[$k])) { return false; }
        }
        return true;
    }
}

if(!function_exists("vgc_display_social_share_widget"))
{
    function vgc_display_social_share_widget()
    {
        do_action( 'addthis_widget', $url, $title, ['size'=>32,'type'=>'custom_toolbox','services'=>'facebook,twitter,google_plusone_share,more,counter']);
    }
}

if(!function_exists("voterguide_get_response_count"))
{
    function voterguide_get_response_count(array $data)
    {
        $response_count = 0;
        foreach($data as $response) {
            if(preg_match("#^(S|O)#ismu",$response->response)) {
                $response_count++;
            }
        }
        return $response_count;
    }
}

if(!function_exists("voterguide_get_comment_count"))
{
    function voterguide_get_comment_count(array $data)
    {
        $comment_count = 0;
        foreach($data as $response) {
            if($response->comment && trim($response->comment)!=="*") {
                $comment_count++;
            }
        }
        return $comment_count;
    }
}

if(!function_exists("voterguide_get_notes_count"))
{
    function voterguide_get_note_count(array $data)
    {
        $note_count = 0;
        foreach($data as $response) {
            if($response->notes) {
                foreach($response->notes as $k => $note) {
                    if(trim($note)!=="*") {
                        $note_count++;
                    }
                }
            }
        }
        return $note_count;
    }
}

// Lets make some shortcodes
include(get_template_directory().'/shortcode_maker.php');

// Strip P and BR tags for shortcodes
remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 12);


//Customize Wordpress Admin

    // add login logo
    function custom_loginlogo() {
    echo '<style type="text/css">
    h1 a {
        height: 100% !important;
        width:100% !important;
        background-image: url('.get_bloginfo('template_directory').'/img/logo-login.png) !important;
        background-postion-x: center !important;
        background-size:100% !important;
        margin-bottom:10px !important; }

    h1 {
        width: 320px !important;
        Height: 120px !important}

    </style>';
    }

    add_action('login_head', 'custom_loginlogo');


    // add custom footer text
    function modify_footer_admin () {
        echo 'Created by <a href="http://factor1studios.com">factor1</a>.';
        echo 'Powered by<a href="http://WordPress.org">WordPress</a>.';
        }

    add_filter('admin_footer_text', 'modify_footer_admin');

/// Dump the yoast SEO columns that are ugly and messy
add_filter( 'wpseo_use_page_analysis', '__return_false' );

// Clean up the <head>
    function removeHeadLinks() {
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
    }

// Remove WP version from html header
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');




if(!function_exists("vgc_get_footer_menu"))
{
    function vgc_get_footer_menu()
    {
        wp_nav_menu([
            'theme_location'=>'footer',
            'menu'=>'',
            'container'=>'div',
            'container_class'=>'row',
            'container_id'=>'',
            'menu_class'=>'small-block-grid-1 medium-block-grid-2 large-block-grid-3',
            'menu_id'=>'',
            'echo'=>true,
            'fallback_cb'=>function() {
                echo "<div class=\"row\">".
                                "<ul class=\"small-block-grid-1 medium-block-grid-2 large-block-grid-3\">".
                                    "<li><a href=\"#\">Register to Vote</a></li>".
                                    "<li><a href=\"#\">Search</a></li>".
                                    "<li><a href=\"#\">About</a></li>".
                                    "<li><a href=\"#\">Get the Guide</a></li>".
                                    "<li><a href=\"#\">Key Dates</a></li>".
                                    "<li><a href=\"#\">Updates</a></li>".
                                "</ul>".
                            "</div>";
            },
            'before'=>'',
            'after'=>'',
            'link_before'=>'',
            'link_after'=>'',
            'items_wrap'=>'<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'=>0,
            'walker'=>''
        ]);
    }
}

if(!function_exists("vgc_get_first_post"))
{
    function vgc_get_first_post(array $args)
    {
        if(!isset($args['posts_per_page'])||$args['posts_per_page']!==1) {
            $args['post_per_page'] = 1;
        }
        return ($posts=get_posts($args)) ? $posts[0] : null;
    }
}

if(!function_exists("vgc_get_ad_image_url"))
{
    function vgc_get_ad_image_url(WP_Post &$post,$field,$size = [300,300])
    {
        return ($img=wp_get_attachment_image_src(get_field($field,$post),$size)) ? $img[0] : null;
    }
}

if(!function_exists("is_single_survey"))
{
    function is_single_survey()
    {
        global $wp_query;
        return $wp_query->is_single_survey;
    }
}

if(!function_exists("is_survey_list"))
{
    function is_survey_list()
    {
        global $wp_query;
        return $wp_query->is_survey_list;
    }
}

if(!function_exists("is_single_candidate"))
{
    function is_single_candidate()
    {
        global $wp_query;
        return $wp_query->is_single_candidate;
    }
}

if(!function_exists("is_candidate_list"))
{
    function is_candidate_list()
    {
        global $wp_query;
        return $wp_query->is_candidate_list;
    }
}

if(!function_exists("is_voter_guide"))
{
    function is_voter_guide()
    {
        global $wp_query;
        return $wp_query->is_voter_guide;
    }
}

if(!function_exists("vgc_get_questions_by_year_type"))
{
    function vgc_get_questions_by_year_type($year,$survey_type)
    {
        return VoterGuideCandidates\VGC_DB::getSurveyQuestions($year,$survey_type);
    }
}

register_nav_menus([
    'footer'=>'Footer Menu'
]);
