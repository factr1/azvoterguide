<?php
/*
Template Name: Voter Guide
*/

$query = $wp_query->query;

$data_years = vgc_get_all_years();
$latest_year = end($data_years);

if(!$data_years||!array_key_exists("post_type",$query)||$query['post_type']!=="vgc_candidate") {
    header("Location: /");
    exit;
}
if(!array_key_exists("vgc_year",$query)||!array_key_exists("vgc_senate_district",$query)||!array_key_exists("vgc_house_district",$query)) {
    header("Location: /candidates/".$latest_year."/");
    exit;
}

get_header();
$breadcrumbs = voterguide_get_breadcrumbs();

$category = vgc_get_post_category();

$voter_guide = vgc_get_voter_guide_list();

$surveys = [
    "Federal"=>vgc_get_questions_by_year_type($latest_year,"Federal"),
    "State"=>vgc_get_questions_by_year_type($latest_year,"State")
];

?>
<section class="blue content-title">
    <div class="row">
        <div class="large-8 columns small-text-center large-text-left">
            <h1>Your Personalized Voter Guide</h1>
        </div>
        <div class="large-4 columns small-text-center large-text-left districts">
            <h2>Congressional District <?php echo $query['vgc_congress_district']; ?></h2>
            <?php if($query['vgc_senate_district']===$query['vgc_house_district']) { ?>
            <h2>Legislative District <?php echo $query['vgc_senate_district']; ?></h2>
            <?php } else { ?>
            <h2>Senate District <?php echo $query['vgc_senate_district']; ?></h2>
            <h2>House District <?php echo $query['vgc_house_district']; ?></h2>
            <?php } ?>
        </div>
    </div>
</section>

<?php if($breadcrumbs) { ?>
<section>
    <div class="row">
        <div class="small-12 columns">
            <div class="breadcrumbs"><?php echo $breadcrumbs['html']; ?></div>
        </div>
    </div>
</section>
<?php } ?>

<section class="candidate voter-guide footerbars">
    <div class="row">
        <div class="large-4 columns large-right">

            <div class="panel guide-menu">

                <div class="row">

                    <div class="medium-6 large-12 columns">
                        <ul class="side-nav state-offices">
                            <?php foreach($voter_guide as $office => $candidates) { ?>
                            <li>
                                <a href="#<?php echo sanitize_title($office); ?>"><?php echo esc_html($office); ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>

                    <div class="medium-6 large-12 columns">
                        <p class="commentnote">
                            Click on the candidate's name to view their individual page and read their comments.
                        </p>
                        <h3>Survey Response Legend</h3>
                        <ul class="no-bullet response-legend">
                            <li><span>S</span>Supports</li>
                            <li><span>O</span>Opposes</li>
                            <li><span>&minus;</span>Declined to respond</li>
                            <li><span>*</span>Comment</li>
                            <li><span>S*</span>Supports with comment</li>
                            <li><span>O*</span>Opposes with comment</li>
                            <li><span>S<sup>&#0134;</sup></span>Supports based on citations</li>
                            <li><span>O<sup>&#0134;</sup></span>Opposes based on citations</li>
                        </ul>
                        <h3>Political Party Legend</h3>
                        <ul class="no-bullet response-legend">
                            <li><span>A</span>Americans Elect</li>
                            <li><span>D</span>Democratic</li>
                            <li><span>I</span>Independent/Other</li>
                            <li><span>L</span>Libertarian</li>
                            <li><span>R</span>Republican</li>
                        </ul>
                        <a href="#" class="button small success print">Print this Guide</a><br />
                        <a class="button small success share-guide">Share this Guide</a><br />
                        <div class="share-widget hide">
                            <?php vgc_display_social_share_widget(); ?>
                        </div>
                        <a href="<?php echo getRequestURI(true); ?>" class="button small success bookmark" title="My Personalized Voter Guide" rel="sidebar">Bookmark this Guide</a>
                    </div>

                </div>

            </div>

        </div>
        <div class="large-8 columns large-left">

            <?php foreach($surveys as $survey_type => $question_list) { ?>

                <div class="row question-list">
                    <div class="small-12 columns">
                        <h1><?php echo $survey_type; ?> Candidate Survey Questions</h1>
                        <ul class="no-bullet">
                            <?php foreach($question_list as $question) { ?>
                            <li><span><?php echo $question->number."."; ?></span><?php echo esc_html($question->text); ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

                <?php foreach($voter_guide as $office => $candidates) { ?>

                    <?php if(!$candidates||$candidates[0]->survey_type!==$survey_type) { continue; } ?>

                    <a name="<?php echo sanitize_title($office); ?>"></a>

                    <div class="state-office">

                        <h1>Candidates Running for <?php echo esc_html($office); ?></h1>

                        <div class="row survey-number-note">
                            <div class="right">
                                <span><span class="show-for-touch">Tap</span><span class="hide-for-touch">Click</span> each number to read the statement</span>
                            </div>
                        </div>
                        <div class="row survey-responses blue">
                            <div class="small-12 medium-4 columns">
                                <a href="#">Survey Questions</a>
                            </div>
                            <div class="small-12 medium-8 columns">
                                <ul class="inline-list medium-right">
                                    <?php foreach($candidates[0]->data as $response) { ?>
                                    <li>
                                        <span class="question-tip" data-title="<?php echo esc_html($response->number.". ".$response->question); ?>"><?php
                                        echo $response->number; ?></span>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>

                        <?php foreach($candidates as $candidate) { ?>

                            <div class="row survey-responses">
                                <div class="small-12 medium-4 columns">
                                    <a href="<?php echo esc_html($candidate->link); ?>">
                                        <?php echo esc_html($candidate->name).(($candidate->party)?" (".substr(ucfirst($candidate->party),0,1).")":""); ?>
                                    </a>
                                </div>

                                <?php if(voterguide_get_response_count($candidate->data)||$candidate->hide_no_response_box) { ?>

                                    <div class="small-12 medium-8 columns">
                                        <ul class="inline-list medium-right">
                                            <?php foreach($candidate->data as $response) { ?>
                                                <li><?php echo $response->response; ?></li>
                                            <?php } ?>
                                        </ul>
                                    </div>

                                <?php } else { ?>

                                    <div class="small-12 columns">
                                        <div class="row no-response">
                                            <div class="large-8 columns">
                                                <h5>Candidate declined to respond</h5>
                                                <p>
                                                    <small>
                                                        Center for Arizona Policy may be providing information about how this candidate would have responded to the survey in the coming days. If you'd like to contact the candidate and ask them to respond to the survey, click to send them an email.
                                                    </small>
                                                </p>
                                            </div>
                                            <?php if($candidate->link) { ?>
                                            <div class="large-4 columns text-right">
                                                <a href="#" class="button success small" data-post-id="<?php echo $candidate->post_id; ?>"
                                                data-survey-type="<?php echo strtolower($candidate->survey_type); ?>">Contact Candidate</a>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                <?php } ?>

                            </div>

                        <?php } ?>

                    </div>

                <?php } ?>
            <?php } ?>
        </div>
    </div>
</section>





<script type="text/javascript">
(function($) {
    $(document).ready(function() {

        var voterGuide$ = $(".voter-guide");
        var guideMenu$ = $(".guide-menu");

        voterGuide$.css("height",voterGuide$.outerHeight()+"px");
        guideMenu$.attr("data-top",guideMenu$.offset().top);

        $(window).on('resize scroll',function() {
            if(voterGuide$.outerHeight()<1400) {
                return false;
            }
            if(guideMenu$.is(".guide-menu:hover")) {
                return false;
            }
            if($(window).width()>1024 && $(window).scrollTop()>=guideMenu$.attr("data-top")) {
                guideMenu$.css("width",guideMenu$.outerWidth());
                guideMenu$.css("height",$(window).height());
                guideMenu$.css("overflow","auto");
                guideMenu$.css("position","fixed");
                guideMenu$.css("top","0px");
                guideMenu$.css("right","0px");
            } else {
                guideMenu$.css("width","auto");
                guideMenu$.css("height","auto");
                guideMenu$.css("overflow","normal");
                guideMenu$.css("position","static");
                guideMenu$.css("top","auto");
                guideMenu$.css("right","auto");
            }
        });

        $("a.share-guide").click(function() {
            $("div.share-widget").find("a[title='View more services']").click();
        });

        $(".button.print").click(function() {
            window.print();
        });

        $(".button.bookmark").click(function(e) {
            if(!window.sidebar) {
                e.preventDefault();
            }
            addToFavorites();
        });

    });
})(jQuery);
</script>

<?php

get_footer();