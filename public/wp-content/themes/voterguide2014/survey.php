<?php
/*
Template Name: Survey
*/

get_header();
$breadcrumbs = voterguide_get_breadcrumbs();

$survey =& get_queried_object();

$data = vgc_get_survey_data($survey);

?>

<section class="blue content-title">
    <div class="row">
        <div class="small-12 columns">
            <h1><?php echo the_title(); ?></h1>
        </div>
    </div>
</section>

<?php if($breadcrumbs) { ?>
<section>
    <div class="row">
        <div class="small-12 columns">
            <div class="breadcrumbs"><?php echo $breadcrumbs['html']; ?></div>
        </div>
    </div>
</section>
<?php } ?>

<section class="survey footerbars">
    <div class="row">
        <div class="large-12 columns text-center">

            <ul class="no-bullet">
                <?php foreach($data as $order => $question) { ?>
                   <li><?php echo "<span>".$order.".</span>".esc_html($question); ?></li>
                <?php } ?>
            </ul>

        </div>
    </div>
</section>

<?php

get_footer();