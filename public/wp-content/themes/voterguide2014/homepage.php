<?php
/*
Template Name: Homepage
*/

get_header();

$service_data = vgc_get_candidate_service_ajax_data();

?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>			
	<section class="row">
	<div class="medium-12 columns">
			<div class="entry">
				<?php the_content(); ?>
			</div>
	</div>
	</section>
<?php endwhile; endif; ?>	



<article>



    <div class="row">

        <div class="large-4 columns text-center">
            <img src="<?php echo get_template_directory_uri(); ?>/img/yourVoiceBadge.png" alt="" title="" />
        </div>

        <div class="large-8 columns">

            <div data-alert class="alert-box warning radius hide">
                <span>test</span>
                <a href="#" class="close">&times;</a>
            </div>

            <form id="frmGetVoterGuide">
                <div class="row">
                    <div class="small-6 columns">
                        <label>
                            First Name* <input type="text" name="first_name" value="" placeholder="First Name" maxlength="100" required />
                        </label>
                    </div>
                    <div class="small-6 columns">
                        <label>
                            Last Name* <input type="text" name="last_name" value="" placeholder="Last Name" maxlength="100" required />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Street Address* <input type="text" name="street_address" value="" placeholder="Street Address" maxlength="100" required />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-5 medium-7 columns">
                        <label>
                            City * <input type="text" name="city" value="" placeholder="City" maxlength="100" required />
                        </label>
                    </div>
                    <div class="small-3 medium-2 columns">
                        <label>
                            State * <input type="text" name="state" value="AZ" placeholder="State" maxlength="100" required />
                        </label>
                    </div>
                    <div class="small-4 medium-3 columns">
                        <label>
                            Zip * <input type="number" name="zip" value="" placeholder="Zip" maxlength="20" digits required />
                        </label>
                    </div>
                    <div class="small-6 medium-2 columns hide">
                        <label>
                            Plus 4 <input type="number" name="plus_4" value="" digits minlength="4" maxlength="4" placeholder="Plus 4" />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <label>
                            Email* <input type="email" name="email" value="" email placeholder="Email" maxlength="255" required />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>
                            <input type="checkbox" name="policy_updates" value="" checked="checked" /><span>Receive email updates about the election.</span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <a href="#" class="button expand"><i class="fa fa-flag fa-lg small-hide medium-show"></i>Get My Personalized Voter Guide</a>
                    </div>
                </div>
            </form>

        </div>

    </div>

</article>

<section class="blue">
    <div class="row">
        <div class="medium-4 columns">
            <a href="#" data-href="https://servicearizona.com/webapp/evoter/selectLanguage" class="new-window"><i class="fa fa-bookmark fa-2x"></i> Register to Vote</a>
        </div>
        <div class="medium-4 columns">
            <a href="/key-dates/"><i class="fa fa-calendar fa-2x"></i> Key Dates</a>
        </div>
        <div class="medium-4 columns">
            <a href="/candidates/"><i class="fa fa-search fa-2x fa-flip-horizontal"></i> List of Candidates</a>
        </div>
    </div>
</section>




<section class="white footerbars">
    <div class="row primaryicons">
        <div class="medium-6 large-4 columns">
            <a href="/candidates/2014/cities/az/" data-href="/candidates/2014/cities/az/"><i class="icon-large-city"></i>City Election Guide</a>
        </div>
        <div class="medium-6 large-4 columns">
            <a href="/download-resources/"><i class="icon-large-download"></i>Download Election Resources</a>
        </div>
        <div class="medium-12 large-4 columns">
            <a href="/common-questions/"><i class="icon-large-questions"></i>Common Questions</a>
        </div>
    </div>

    <div class="row secondaryicons">
     <div class="medium-6 large-4 columns">
            <a href="#" data-href="https://voter.azsos.gov/VoterView/PollingPlaceSearch.do" class="new-window"><i class="fa fa-map-marker"></i><br>Find your Polling Location</a>
        </div>
        <div class="medium-6 large-4 columns">
            <a href="#" data-href="https://azpolicy.wufoo.com/forms/voter-guide-request-form/" class="new-window"><i class="fa fa-tasks"></i><br>Order Election Resources</a>
        </div>
        <div class="medium-12 large-4 columns">
            <a href="/blog/"><i class="icon-small-blog"></i><br>azvoterguide.com blog</a>
        </div>
    </div>

    <div class="row">
        <div class="share-widget medium-3 medium-centered columns">
        <center><h3>Share azvoterguide.com</h3>
        <?php vgc_display_social_share_widget(); ?></center>
        </div>
    </div>


</section>

<script type="text/javascript">
(function($) {

    $(document).ready(function() {

        html$ = $("html");
        body$ = $("body");
        form$ = $("#frmGetVoterGuide");
        article$ = form$.parents("article");
        msg$ = $(".alert-box").find("span");

        form$.validate();

        form$.find(".button").click(function() {

            msg$.parent().toggle(false);

            if(form$.valid()) {

                div$ = $(document.createElement("div")).addClass("form-processing");

                icon$ = div$.clone();
                icon$
                    .css("background-color","transparent")
                    .css("opacity","1")
                    .html("<i class=\"fa fa-spinner fa-spin fa-5x\"></i>");

                var set_processing_positions = function() {
                    margins = parseInt(html$.css("margin-top"))+parseInt(body$.css("margin-top"));
                    paddings = parseInt(html$.css("padding-top"))+parseInt(body$.css("padding-top"));
                    div$.css("top",article$.offset().top-(margins+paddings));
                    div$.css("height",article$.outerHeight());
                    icon$.css("top",div$.css("top"));
                    icon$.css("height",div$.css("height"));
                };
                set_processing_positions();

                body$.append(div$).append(icon$);

                $(window).resize(function() { set_processing_positions(); });

                $.post('<?php echo $service_data['ajax_url']; ?>',{
                    action: "<?php echo $service_data['ajax_action']; ?>",
                    <?php echo $service_data['ajax_token_name']; ?>: "<?php echo $service_data['ajax_nonce']; ?>",
                    first_name: form$.find("input[name='first_name']").val(),
                    last_name: form$.find("input[name='last_name']").val(),
                    street_address: form$.find("input[name='street_address']").val(),
                    city: form$.find("input[name='city']").val(),
                    state: form$.find("input[name='state']").val(),
                    zip: form$.find("input[name='zip']").val(),
                    plus_4: form$.find("input[name='plus_4']").val(),
                    email: form$.find("input[name='email']").val(),
                    policy_updates: (form$.find("input[name='policy_updates']").is(":checked")?"Y":"N")
                },function(data) {
                    if(data.result===true) {
                      location.href = "/voter-guides/"+data.congress_district+"/"+data.senate_district+"/"+data.house_district+"/";
                      return;
                    }
                    var msg = (data.user_message) ? data.user_message : 'An unexpected error occurred.';
                    msg$.html(msg).parent().toggle(true);
                },'json')
                .fail(function(jqXHR,textStatus,errorThrown ) {
                    msg$.html(errorThrown).parent().toggle(true);
                })
                .always(function() {
                    div$.remove();
                    icon$.remove();
                });
            }
        });

    });

})(jQuery);
</script>
<?php

get_footer();