<?php
/*
Template Name: Candidate
*/

get_header();
$breadcrumbs = voterguide_get_breadcrumbs();

$candidate =& get_queried_object();

$survey_type = vgc_get_post_term($candidate,"vgc_survey_type");

$data = vgc_get_candidate_data($candidate);

?>

<section class="blue content-title">
    <div class="row">
        <div class="small-12 columns">
            <h1>Meet the Candidate</h1>
        </div>
    </div>
</section>

<?php if($breadcrumbs) { ?>
<section>
    <div class="row">
        <div class="small-12 columns">
            <div class="breadcrumbs"><?php echo $breadcrumbs['html']; ?></div>
        </div>
    </div>
</section>
<?php } ?>

<section class="candidate footerbars">
    <div class="row">
        <div class="medium-5 large-4 columns">

            <?php if($img_url=vgc_get_candidate_photo_url($candidate)) { ?>
            <p class="text-center">
                <img src="<?php echo $img_url; ?>"
                alt="<?php echo esc_html($candidate->post_title); ?>"
                title="<?php echo esc_html($candidate->post_title); ?>" />
            </p>
            <?php } else { ?>
            <p class="text-center">
                <img src="<?php bloginfo('template_url'); ?>/img/nophoto.png"
                alt="<?php echo esc_html($candidate->post_title); ?>"
                title="<?php echo esc_html($candidate->post_title); ?>" />
            </p>
            <?php } ?>

            <div class="row connect">
                <div class="small-3 columns">
                    <h2>Connect</h2>
                </div>
                <div class="small-7 medium-offset-2 columns">
                    <ul class="small-block-grid-4 connect-grid">
                        <?php if($field=get_field('website',$candidate)) { ?>
                            <li><a href="#" data-href="<?php echo esc_html($field); ?>" class="new-window"><i class="fa fa-globe fa-2x"></i></a></li>
                        <?php } ?>
                        <?php if($field=get_field('email',$candidate)) { ?>
                            <li><a href="mailto:<?php echo esc_html($field); ?>"><i class="fa fa-envelope fa-2x"></i></a></li>
                        <?php } ?>
                        <?php if($field=get_field('facebook_url',$candidate)) { ?>
                            <li><a href="#" data-href="<?php echo esc_html($field); ?>" class="new-window"><i class="fa fa-facebook fa-2x"></i></a></li>
                        <?php } ?>
                        <?php if($field=get_field('twitter_url',$candidate)) { ?>
                            <li><a href="#" data-href="<?php echo esc_html($field); ?>" class="new-window"><i class="fa fa-twitter fa-2x"></i></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>

            <?php if($field=get_field('phone',$candidate)) { ?>
            <div class="row">
                <div class="medium-3 columns"></div>
                <div class="medium-7 medium-offset-2 columns">
                    <span>Phone: <?php echo esc_html(vgc_format_phone($field)); ?></span>
                </div>
            </div>
            <?php } ?>

            <div class="row share">
                <div class="medium-3 columns">
                    <h2>Share</h2>
                </div>
                <div class="medium-9 columns">
                    <div class="share-widget">
                        <?php vgc_display_social_share_widget(); ?>
                    </div>
                </div>
            </div>

            <?php if($data) { ?>
            <h3>Survey Response Legend</h3>
            <ul class="no-bullet response-legend">
                <li><span>S</span>Supports</li>
                <li><span>O</span>Opposes</li>
                <li><span>&minus;</span>Declined to respond</li>
                <li><span>*</span>Comment</li>
                <li><span>S*</span>Supports with comment</li>
                <li><span>O*</span>Opposes with comment</li>
                <li><span>S<sup>&#0134;</sup></span>Supports based on citations</li>
                <li><span>O<sup>&#0134;</sup></span>Opposes based on citations</li>
            </ul>

            <h3>Political Party Legend</h3>
                        <ul class="no-bullet response-legend">
                            <li><span>A</span>Americans Elect</li>
                            <li><span>D</span>Democratic</li>
                            <li><span>I</span>Independent/Other</li>
                            <li><span>L</span>Libertarian</li>
                            <li><span>R</span>Republican</li>
                        </ul>
            <?php } ?>

        </div>
        <div class="medium-7 large-8 columns candidate-info">

            <h2><?php echo esc_html($candidate->post_title).(($party=get_field("party",$candidate))?" (".substr(ucfirst($party),0,1).")":""); ?></h2>

            <p>
                <span>Running for:</span><a href="<?php echo vgc_get_candidate_category_slug($candidate); ?>">
                    <?php echo esc_html(vgc_get_candidate_office($candidate)); ?>
                </a>
            </p>

            <?php if($district=vgc_get_candidate_district($candidate)) { ?>
            <p>
                <span>District:</span><?php echo $district; ?>
            </p>
            <?php } ?>

            <?php if($field=get_field('appointed_by',$candidate)) { ?>
            <p>
                <span>Appointed By:</span><br />
                <?php echo esc_html($appointed_by); ?>
            </p>
            <?php } ?>

            <?php if($field=get_field('judicial_score',$candidate)) { ?>
            <p>
                <span>Judicial Performance Review Score:</span><br />
                <?php echo esc_html($judicial_score); ?>
            </p>
            <?php } ?>

            <?php if($age=get_field('age',$candidate)) { ?>
            <p>
                <span>Age:</span><?php echo $age; ?>
            </p>
            <?php } ?>

            <?php if($occupation=get_field('occupation',$candidate)) { ?>
            <p>
                <span>Occupation:</span><?php echo esc_html($occupation); ?>
            </p>
            <?php } ?>

            <?php if($family=get_field('family',$candidate)) { ?>
            <p>
                <span>Family:</span><?php echo esc_html($family); ?>
            </p>
            <?php } ?>

            <?php if($religion=get_field('religion',$candidate)) { ?>
            <p>
                <span>Religious Preference:</span><?php echo esc_html($religion); ?>
            </p>
            <?php } ?>

            <?php if($statement=get_field('statement',$candidate)) { ?>
            <p>
                <span>Why are you running?</span><br />
                <?php echo $statement; ?>
            </p>
            <?php } ?>

            <br />

            <?php if($data) { ?>

            <h2 id="Questions">
                <div class="click-to-expand"><a href="#Questions" data-toggle=".question-list">Click to expand</a></div>
                <span data-toggle=".question-list">Survey Questions</span>
            </h2>

            <div class="row question-list">
                <div class="small-12 columns">
                    <ul class="no-bullet">
                        <?php foreach($data as $response) { ?>
                        <li><span><?php echo $response->number."."; ?></span><?php echo esc_html($response->question); ?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>

            <h2>Survey Responses</h2>

            <div class="row survey-number-note">
                <div class="right">
                    <span><span class="show-for-touch">Tap</span><span class="hide-for-touch">Click</span> each number to read the statement</span>
                </div>
            </div>
            <div class="row survey-responses blue">
                <div class="small-12 medium-4 columns">
                    <a href="#">Survey Questions</a>
                </div>
                <div class="small-12 medium-8 columns">
                    <ul class="inline-list medium-right">
                        <?php foreach($data as $response) { ?>
                        <li>
                            <span class="question-tip" data-title="<?php echo esc_html($response->number.". ".$response->question); ?>"><?php echo
                            $response->number; ?></span>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="row survey-responses">
                <div class="small-12 medium-4 columns">
                    <?php echo esc_html($candidate->post_title).(($party=get_field("party",$candidate))?" (".substr(ucfirst($party),0,1).")":""); ?>
                </div>
                <div class="small-12 medium-8 columns">
                    <ul class="inline-list medium-right">
                        <?php foreach($data as $response) { ?>
                            <li><?php echo $response->response; ?></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <?php } ?>

            <?php if(!voterguide_get_response_count($data)&&!get_field("hide_no_response_box",$candidate)) { ?>
            <div class="row">
                <div class="small-12 columns">
                    <div class="row no-response">
                        <div class="large-8 columns">
                            <h5>Candidate declined to respond</h5>
                            <p>
                                <small>
                                    This candidate declined to respond to the the Voter Guide survey. If you'd like to know where the
                                    candidate stands on these issues, click to send them an email asking for their responses.
                                </small>
                            </p>
                        </div>
                        <?php if($field=get_field('email',$candidate)) { ?>
                        <div class="large-4 columns text-right">
                            <a href="#" class="button success small" data-post-id="<?php echo $candidate->ID; ?>"
                            data-survey-type="<?php echo strtolower($survey_type); ?>">Contact Candidate</a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>

            <?php if(voterguide_get_comment_count($data)) { ?>
            <h2>Candidate Comments</h2>
            <ul class="no-bullet comments">
                <?php foreach($data as $response) { if($response->comment && trim($response->comment)!=="*") { ?>
                <li>
                    <span><?php echo "Question ".$response->number.":"; ?></span>
                    <?php echo $response->comment; ?>
                </li>
                <?php } } ?>
            </ul>
            <?php } ?>

            <?php if(voterguide_get_note_count($data)) { ?>
            <h2>Notes</h2>
            <ul class="no-bullet notes">
                <?php foreach($data as $response) { if($response->notes) { foreach($response->notes as $k => $note) {  if(trim($note)!=="*") { ?>
                <li>
                    <span><?php echo "Question ".$response->number.":"; ?></span>
                    <?php echo $response->notes[$k]; ?>
                </li>
                <?php } } } } ?>
            </ul>
            <?php } ?>

            <?php if($notes=vgc_get_candidate_other_notes($candidate)) { ?>
            <ul class="no-bullet notes">
                <?php foreach($notes as $note) { if($note) { ?>
                <li>
                    <?php echo $note->text; ?>
                </li>
                <?php } } ?>
            </ul>

            <?php } ?>

        </div>
    </div>
</section>

<?php

get_footer();