<form class="candidate-search"  data-year="<?php echo esc_html(get_query_var("vgc_year")); ?>" >
    <div class="row">
        <div class="small-8 medium-6 large-9 columns">
            <input type="text" placeholder="Search Candidates" value="<?php echo esc_html(get_query_var("s")); ?>" />
        </div>
        <div class="small-4 medium-6 large-3 columns">
            <a href="#" class="button tiny expand"><i class="fa fa-search"></i> Search</a>
        </div>
    </div>
</form>