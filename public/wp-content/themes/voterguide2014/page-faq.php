<?php
/*
Template Name: FAQ detail
*/

get_header();

?>

<section class="blue content-title">
    <div class="row">
        <div class="small-12 columns">
            <h1><?php echo the_title(); ?></h1>
        </div>
    </div>
</section>

<section class="page-content footerbars">
    <div class="row">
        <div class="large-12 columns">
            <?php the_content(); ?>

            <a class="button button-navy" id="btn_faq_openall">Open all</a>  <a class="button button-navy" id="btn_faq_closeall">Close all</a>

            <?php if(get_field('faq_content')) { ?>
            <div class="faqitems">
                <?php while(has_sub_field('faq_content')) { ?>
                <div class="faq_item item_status_closed">
                    <h3 class="faq_toggler"><i class="fa fa-minus open"></i> <i class="fa fa-plus closed"></i> <?php the_sub_field('faq_item_title');?></h3>
                    <div class="faq_details">
                        <p><?php the_sub_field('faq_item_detail');?></p>
                        <p><a href="#" class="top">Back to top <i class="fa fa-angle-up"></i></a></p>
                    </div>
                </div>
                <?php } ?>
            </div>
            <?php } ?>

            <?php if(get_field('closing_copy')) { the_field('closing_copy'); } ?>

        </div>
    </div>
</section>

<style type="text/css">
    .faq_details {
        display:none;
    }
</style>
<script type="text/javascript">
    (function($) {
        $('#btn_faq_closeall').on('click', function() {
            $('.faq_item').removeClass('item_status_open').addClass('item_status_closed');
            $('.faq_details').slideUp();
        });
        $('#btn_faq_openall').on('click', function() {
            $('.faq_item').removeClass('item_status_closed').addClass('item_status_open');
            $('.faq_details').slideDown();
        });
        $('.faq_toggler').on('click', function() {
            var parent = $(this).parent();
            if($(parent).hasClass('item_status_open')) {
                $(this).siblings('.faq_details').slideUp(function() {
                    $(parent).removeClass('item_status_open').addClass('item_status_closed');
                });
            } else {
                $(parent).addClass('item_status_open').removeClass('item_status_closed');
                $(this).siblings('.faq_details').slideDown();
            }
        })
    })(jQuery);
</script>

<?php

get_footer();
