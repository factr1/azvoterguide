<?php $service_data = vgc_get_candidate_message_ajax_data(); ?>

<div id="modalContactCandidate" class="reveal-modal medium" data-reveal>
    <h2>Contact Candidate</h2>
    <div data-alert class="alert-box warning radius hide">
        <span></span>
        <a href="#" class="close">&times;</a>
    </div>
    <div class="row message-form">
        <form>
            <input type="hidden" name="post_id" value="" digits required />
            <div class="large-6 columns">
                <label>
                    Your Name* <input type="text" name="name" value="" placeholder="Name" maxlength="100" required />
                </label>
            </div>
            <div class="large-6 columns">
                <label>
                    Your Email* <input type="email" name="email" value="" placeholder="Email" maxlength="255" required email />
                </label>
            </div>
            <div class="small-12 columns">
                <label>
                    <textarea name="message" class="hide" required></textarea>
                    <div class="textarea"></div>
                    <div class="message-federal hide"><?php get_template_part('candidate-message-federal'); ?></div>
                    <div class="message-state hide"><?php get_template_part('candidate-message-state'); ?></div>
                    <div class="message-counties hide"><?php get_template_part('candidate-message-counties'); ?></div>
                    <div class="message-cities hide"><?php get_template_part('candidate-message-cities'); ?></div>
                    <div class="message-school-boards hide"><?php get_template_part('candidate-message-school-boards'); ?></div>
                    <div class="message-judges hide"><?php get_template_part('candidate-message-judges'); ?></div>
                </label>
            </div>
            <div class="small-12 columns">

                <?php echo get_template_part('widget-recaptcha'); ?>

                <?php /* echo vgc_captcha_html("clean"); */ ?>

                <br />
                <br />
            </div>
            <div class="small-12 columns">
                <button class="button success small send-message" type="button">Send Message</button>
                <button class="button secondary small cancel" type="button">Cancel</button>
            </div>
        </form>
    </div>
    <div class="row thank-you hide">
        <div class="small-12 columns">
            <p>Your message was successfully sent. Thank You!</p>
        </div>
        <div class="small-12 columns">
            <button class="button secondary small cancel" type="button">Close</button>
        </div>
    </div>
</div>

<script type="text/javascript">
(function($) {

    $(document).ready(function() {

        var html$ = $("html");
        var body$ = $("body");
        var modal$ = $("#modalContactCandidate");
        var error$ = modal$.find(".alert-box").find("span");
        var form$ = modal$.find("form");
        var post_id$ = form$.find("input[name='post_id']");
        var name$ = form$.find("input[name='name']");
        var email$ = form$.find("input[name='email']");
        var message$ = form$.find("textarea[name='message']");
        var recaptcha_response$ = form$.find("input[name='recaptcha_response_field']");

        modal$.find(".cancel").click(function() { modal$.foundation('reveal', 'close'); });
        modal$.find(".send-message").click(function() {

            message$.val(message$.next('.textarea').html());

            var recaptcha_challenge$ = form$.find("input[name='recaptcha_challenge_field']");

            if(form$.valid()) {

                div$ = $(document.createElement("div")).addClass("form-processing");

                icon$ = div$.clone();
                icon$
                    .css("background-color","transparent")
                    .css("opacity","1")
                    .html("<i class=\"fa fa-spinner fa-spin fa-5x\"></i>");

                var set_processing_positions = function() {
                    marginsTop = parseInt(html$.css("margin-top"))+parseInt(body$.css("margin-top"));
                    paddingsTop = parseInt(html$.css("padding-top"))+parseInt(body$.css("padding-top"));
                    marginsLeft = parseInt(html$.css("margin-left"))+parseInt(body$.css("margin-left"));
                    paddingsLeft = parseInt(html$.css("padding-left"))+parseInt(body$.css("padding-left"));
                    div$.css("top",modal$.offset().top-(marginsTop+paddingsTop));
                    div$.css("left",modal$.offset().left-(marginsLeft+paddingsLeft));
                    div$.css("height",modal$.outerHeight());
                    div$.css("width",modal$.outerWidth());
                    icon$.css("top",div$.css("top"));
                    icon$.css("left",div$.css("left"));
                    icon$.css("height",div$.css("height"));
                    icon$.css("width",div$.css("width"));
                };
                set_processing_positions();

                body$.append(div$).append(icon$);

                $(window).resize(function() { set_processing_positions(); });

                $.post('<?php echo $service_data['ajax_url']; ?>',{
                    action: "<?php echo $service_data['ajax_action']; ?>",
                    <?php echo $service_data['ajax_token_name']; ?>: "<?php echo $service_data['ajax_nonce']; ?>",
                    post_id: post_id$.val(),
                    name: name$.val(),
                    email: email$.val(),
                    message: message$.val(),
                    recaptcha_challenge_field: recaptcha_challenge$.val(),
                    recaptcha_response_field: recaptcha_response$.val()
                },function(data) {
                    if(data.result===true) {
                        form$.parent().toggle(false).next().toggle(true);
                        return;
                    }
                    var msg = (data.user_message) ? data.user_message : 'An unexpected error occurred.';
                    error$.html(msg).parent().toggle(true);
                },'json')
                .fail(function(jqXHR,textStatus,errorThrown ) {
                    error$.html(errorThrown).parent().toggle(true);
                })
                .always(function() {
                    div$.remove();
                    icon$.remove();
                });

            }
        });

        $(".row.no-response .button").click(function() {
            post_id$.val($(this).attr("data-post-id"));
            message$.next(".textarea").html(modal$.find(".message-"+$(this).attr("data-survey-type")).html());
            modal$.foundation('reveal', 'open');
        });

        $(document).on('opened.fndtn.reveal','[data-reveal]',function() {
            if($(this).is(modal$)) {
                var top = $(window).scrollTop()+($(window).height()/2)-(modal$.height()/2);
                top = (top<0) ? 0 : top;
                modal$.css('top',top+"px");
            }
        });

        $(document).on('closed.fndtn.reveal','[data-reveal]',function() {
            if($(this).is(modal$)) {
                form$.parent().toggle(true).next().toggle(false);
            }
        });

    });

})(jQuery);
</script>