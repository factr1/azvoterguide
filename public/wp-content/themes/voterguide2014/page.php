<?php get_header();
$breadcrumbs = voterguide_get_breadcrumbs();
$ad_block = get_field('ad_image');

?>

<section class="blue content-title">
    <div class="row">
        <div class="small-12 columns">
            <h1><?php echo the_title(); ?></h1>
        </div>
    </div>
</section>

<?php if($breadcrumbs) { ?>
<section>
    <div class="row">
        <div class="small-12 columns">
            <div class="breadcrumbs"><?php echo $breadcrumbs['html']; ?></div>
        </div>
    </div>
</section>
<?php } ?>

<section class="page-content footerbars">
    <div class="row">
        <div class="large-<?php echo (($ad_block)?"7":"12"); ?> columns">
            <?php the_content(); ?>
        </div>
        <?php if($ad_block) { ?>
        <div class="large-5 columns text-center">
            <div class="panel">
                <a href="#" data-href="" class="new-window">
                    <img src="" alt="" title="" />
                </a>
            </div>
        </div>
        <?php } ?>
    </div>
</section>

<?php

get_footer();