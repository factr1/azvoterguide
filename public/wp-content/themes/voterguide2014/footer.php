<footer>
    <div class="row">
        <div class="small-7 medium-5 large-4 columns">
            <a href="#" data-href="http://www.azpolicy.org/" class="new-window">
                <span>Brought to you by:</span><i class="icon-az-policy-logo" title="Center for Arizona Policy"></i>
            </a>
        </div>
        <div class="small-5 medium-6 columns">
            <?php vgc_get_footer_menu(); ?>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

<?php if(is_single_candidate()||is_voter_guide()) { get_template_part("modal-contact-candidate"); } ?>
<?php get_template_part("modal-tooltip"); ?>
</body>
</html>