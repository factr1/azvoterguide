<?php

echo    "<article id=\"post-".the_ID()."\" ".post_class().">".
                "Post Type: ".get_post_type()."<br />".
                "Author Avatar: ".get_avatar(get_the_author_meta('ID'),300)."<br />".
                "Author: ".the_author()."<br />".
                "Permalink: ".the_permalink()."<br />".
                "Title: ".the_title()."<br />".
                "Date: ".the_date()."<br />".
                ((is_search)?
                    "Excerpt: ".the_excerpt()."<br />"
                :
                    "Content: ".the_content()."<br />"
                ).
            "</article>";

?>
<?php do_action( 'addthis_widget', $url, $title); ?>