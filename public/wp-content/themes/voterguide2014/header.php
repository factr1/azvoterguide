<?php

$translator = (shortcode_exists("google-translator")) ? do_shortcode('[google-translator]') : "";

?>
<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie ie6"><![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie ie7"><![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie ie9"><![endif]-->
<!--[if gt IE 9]><html lang="en" class="no-js ie"><![endif]-->
<!--[if !IE]><!--><html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="personal" content="no-cache" />
    <meta name="rating" content="general" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta charset="utf-8" />
    <?php if (is_search()) { ?>
    <meta name="robots" content="noindex, nofollow" />
    <?php } ?>
    <meta property="og:title" content="<?php the_title(); ?>" />
    <meta property="og:description" content="<?php the_excerpt(); ?>" />
    <meta property="og:site_name" content="<?php bloginfo('name') ?>" />
    <title><?php echo voterguide_get_page_title(); ?></title>
    <meta name="author" content="Center for Arizona Policy" />
    <link type="text/plain" rel="author" href="http://www.azvoterguide.com/humans.txt" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php if(@file_exists(get_template_directory()."/favicon.ico")) { ?>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri()."/favicon.ico"; ?>" type="image/x-icon" />
    <?php } ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/foundation/5.2.3/css/foundation.min.css" />
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/foundation/5.2.3/css/normalize.css" />
    <link type="text/css" rel="stylesheet"  href="https://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/print.css" type="text/css" media="print" >

    <script src="https://code.jquery.com/jquery-2.1.0.min.js" type="text/javascript"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.2.3/js/foundation.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.2.2/js/foundation/foundation.offcanvas.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.2.3/js/foundation/foundation.reveal.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/5.2.3/js/foundation/foundation.tooltip.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).foundation();
        });
    </script>
    <script src="<?php echo get_template_directory_uri()."/js/main.js"; ?>" type="text/javascript"></script>
    <script type="text/javascript" src="//use.typekit.net/zzv4jdf.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
    <link rel="apple-touch-icon" href="<?php bloginfo('template_url'); ?>/img/touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_url'); ?>/img/touch-icon.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_url'); ?>/img/touch-icon.png">


</head>
<body <?php body_class(); ?>>

<?php echo $translator; ?>

<header>
    <div class="row">

        <div class="medium-4 columns">
            <h1><a href="/" title="azvoterguide.com">azvoterguide.com</a></h1>
        </div>

        <div class="medium-3 columns upcomingleader">
            <h2><?php the_field( "headline_title", "option" ); ?></h2>
            <h3><?php the_field( "secondary_headline", "option" ); ?></h3>
        </div>

         <div class="medium-5 columns headersearch">
               <form class="candidate-search" action="" data-year="<?php echo esc_html(($year=get_query_var("vgc_year"))?$year:end(vgc_get_all_years())); ?>" >
                    <div class="row">
                        <div class="small-8 medium-6 large-9 columns">
                            <input type="text" placeholder="Search Candidates" value="<?php echo esc_html(get_query_var("s")); ?>" />
                        </div>
                        <div class="small-4 medium-6 large-3 columns">
                            <a href="#" class="button tiny expand"><i class="fa fa-search"></i> Search</a>
                        </div>
                    </div>
                </form>
         </div>

         <div class="medium-1 columns end">
         </div>

    </div>
</header>