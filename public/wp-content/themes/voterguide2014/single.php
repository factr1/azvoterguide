<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<section class="blue content-title">
    <div class="row">
        <div class="small-12 columns">
            <h1><center><?php the_title(); ?></center></h1>
        </div>
    </div>
</section>


<section class="page-content footerbars">
<article class="content row">
<div class="large-8 large-centered columns">
	

		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<?php if(has_post_thumbnail()) {
			the_post_thumbnail();
			} else {	}
			?>


			<div class="entry">
				<?php the_content();?>

			</div>

		</div>

	<?php endwhile; ?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>
</div>
</article>
</section>

<?php get_footer(); ?>