<?php
/*
Template Name: Survey List
*/

$query = $wp_query->query;

$data_years = vgc_get_all_years();
$latest_year = end($data_years);

if(!$data_years||!array_key_exists("post_type",$query)||$query['post_type']!=="vgc_survey") {
    header("Location: /");
    exit;
}
if(!array_key_exists("vgc_year",$query)) {
    header("Location: /surveys/".$latest_year."/");
    exit;
}

get_header();
$breadcrumbs = voterguide_get_breadcrumbs();

$survey_type = vgc_get_post_survey_type();

$link_list = ($survey_type) ? vgc_get_survey_list() : vgc_get_survey_type_list();

?>

<section class="blue content-title">
    <div class="row">
        <div class="small-12 columns">
            <h1>Survey List</h1>
        </div>
    </div>
</section>

<?php if($breadcrumbs) { ?>
<section>
    <div class="row">
        <div class="small-12 columns">
            <div class="breadcrumbs"><?php echo $breadcrumbs['html']; ?></div>
        </div>
    </div>
</section>
<?php } ?>

<section class="candidate-names footerbars">
    <div class="row">
        <div class="large-4 columns">
            <ul class="large-block-grid-1">
                <?php foreach($link_list as $item) { ?>
                <li><a href="<?php echo esc_html($item->link); ?>"><?php echo esc_html($item->name); ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <div class="large-8 columns">
            <h1>test</h1>
            <p>
                Quisque eget odio ac lectus vestibulum faucibus eget in metus. In pellentesque faucibus vestibulum.
                Nulla at nulla justo, eget luctus tortor. Nulla facilisi. Duis aliquet egestas purus in blandit. Curabitur
                vulputate, ligula lacinia scelerisque tempor, lacus lacus ornare ante, ac egestas est urna sit amet
                arcu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed
                molestie augue sit amet leo consequat posuere. Vestibulum ante ipsum primis in.
            </p>
            <div class="panel" style="width:650px;height:300px">
                Ad Space - 650 x 300
            </div>
        </div>
    </div>
</section>

<?php

get_footer();