<?php get_header();?>

<section class="blue content-title">
    <div class="row">
        <div class="small-12 columns">
            <h1>Page not found</h1>
        </div>
    </div>
</section>


<section class="page-content footerbars">
    <div class="row">
        <div class="medium-6 medium-centered columns">
            <h2>Sorry we are unable to locate the page you are looking for.</h2>
            <p style="margin-bottom:100px;">It's possible that you have found an old link or bookmark from a past years guide. We work hard every election year to update our tools and resources to bring you the latest information. Please head to the <a href="/">home page</a> to create a customized voter guide for this season. Thanks!</p>
        </div>
    </div>
</section>



<?php get_footer(); ?>
