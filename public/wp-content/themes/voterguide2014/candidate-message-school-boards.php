<?php

echo    "Before I cast my vote, I'd like to know where you stand on some of the most important issues of our day. ".
            "Please, take time today to complete Center for Arizona Policy's azvoterguide.com candidate survey.<br />".
            "<br />".
             "This is a nonpartisan survey that helps me know how you would lead if elected.<br />".
             "<br />".
             "Click here to complete the survey online: <a href=\"http://azpolicy.org/statesurvey2014\">http://azpolicy.org/statesurvey2014</a><br />".
             "<br />"
             ;