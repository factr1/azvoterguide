<?php

/**
*   Custom Configuration Loader
*/

define('PUBLIC_DIR',str_replace("\\","/",dirname(__FILE__)));
define('VENDOR_DIR',str_replace("\\","/",realpath(PUBLIC_DIR."/../vendor")));

if(!defined('IS_CLI'))
{
    define("IS_CLI",(php_sapi_name()==="cli"||defined('STDIN'))?true:false);
}

if(IS_CLI)
{
    switch(strtolower(PUBLIC_DIR)) {
        case "c:/hostroot/repos/www.azvoterguide.local/public":
        case "/c/hostroot/repos/www.azvoterguide.local/public":
            $_SERVER['HTTP_HOST'] = "www.azvoterguide.local";
            break;
        case "/Users/mattadams/Documents/GIT/AZvoterguide 2014/public":
            $_SERVER['HTTP_HOST'] = "azvoterguide:8888";
            break;
        case "/home/azvoterg/azvoterguide-staging/public":
            $_SERVER['HTTP_HOST'] = "staging.azvoterguide.com";
            break;
        /*
        case "":
            $_SERVER['HTTP_HOST'] = "www.azvoterguide.com";
            break;
        */
        default:
            exit('The application environment is not set correctly.');
    }
}

if(!defined('ENVIRONMENT'))
{
    switch($_SERVER['HTTP_HOST']) {
        case "www.azvoterguide.local":
            define('ENVIRONMENT', 'local');
            break;
        case "azvoterguide:8888":
            define('ENVIRONMENT', 'development');
            break;
        case "staging.azvoterguide.com":
            define('ENVIRONMENT', 'staging');
            break;
        case "production.azvoterguide.com":
        case "azvoterguide.com":
        case "www.azvoterguide.com":
        case "www2.azvoterguide.com":
            define('ENVIRONMENT', 'production');
            break;
        default:
            exit('The application environment is not set correctly.');
    }
}

switch(ENVIRONMENT)
{
    case 'development':
    case 'local':
        error_reporting(E_ALL);
        break;
    case 'staging':
    case 'production':
        error_reporting(0);
        break;
    default:
        exit('The application environment is not set correctly.');
}

$wp_config_file = str_replace("\\","/",realpath(PUBLIC_DIR."/../wp-config.".ENVIRONMENT));

if(!@file_exists($wp_config_file)) {
    exit('The application environment configuration file is missing or unreadable.');
}

require_once($wp_config_file);
